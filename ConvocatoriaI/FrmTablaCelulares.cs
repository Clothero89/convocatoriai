﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvocatoriaI
{
    public partial class FrmTablaCelulares : Form
    {
        private DataSet dsCelular;
        private BindingSource bsCelular;

        public DataSet DsCelular
        {
            get
            {
                return dsCelular;
            }

            set
            {
                dsCelular = value;
            }
        }

        public FrmTablaCelulares()
        {
            InitializeComponent();
            bsCelular = new BindingSource();
        }

        private void FrmTablaCelulares_Load(object sender, EventArgs e)
        {


        }

        private void BtnNetwork_Click(object sender, EventArgs e)
        {

        }

        private void BtnLaunch_Click(object sender, EventArgs e)
        {

        }

        private void BtnBody_Click(object sender, EventArgs e)
        {

        }

        private void BtnDisplay_Click(object sender, EventArgs e)
        {

        }

        private void BtnPlatform_Click(object sender, EventArgs e)
        {

        }

        private void BtnMemory_Click(object sender, EventArgs e)
        {

        }
    }
}
