﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvocatoriaI.Propiedades
{
    class Body
    {
        private int id;
        private string dimensiions;
        private string weight;
        private string build;
        private string sim;

        public string Dimensiions { get => dimensiions; set => dimensiions = value; }
        public string Weight { get => weight; set => weight = value; }
        public string Build { get => build; set => build = value; }
        public string Sim { get => sim; set => sim = value; }
        public int Id { get => id; set => id = value; }

        public Body(int id, string dimensiions, string weight, string build, string sim)
        {
            this.id = id;
            this.dimensiions = dimensiions;
            this.weight = weight;
            this.build = build;
            this.sim = sim;
        }

        public Body()
        {
        }
    }
}
