﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvocatoriaI.Propiedades
{
    class Platform
    {
        private int id;
        private string os;
        private string chipset;
        private string cpu;
        private string gpu;

        public string Os { get => os; set => os = value; }
        public string Chipset { get => chipset; set => chipset = value; }
        public string Cpu { get => cpu; set => cpu = value; }
        public string Gpu { get => gpu; set => gpu = value; }
        public int Id { get => id; set => id = value; }

        public Platform(int id,  string os, string chipset, string cpu, string gpu)
        {
            this.id = id;
            this.os = os;
            this.chipset = chipset;
            this.cpu = cpu;
            this.gpu = gpu;
        }

        public Platform()
        {
        }


    }
}
