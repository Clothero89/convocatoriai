﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvocatoriaI.Propiedades
{
    class Memory
    {
        private int id;
        private string cardSlot;
        private string mInternal;

        public string CardSlot { get => cardSlot; set => cardSlot = value; }
        public string MInternal { get => mInternal; set => mInternal = value; }
        public int Id { get => id; set => id = value; }

        public Memory(int id, string cardSlot, string mInternal)
        {
            this.id = id;
            this.cardSlot = cardSlot;
            this.mInternal = mInternal;
        }

        public Memory()
        {
        }
    }
}
