﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvocatoriaI.Propiedades
{
    class Launch
    {
        private int id;
        private string announced;
        private string status;

        public Launch()
        {
        }

        public Launch(int id, string announced, string status)
        {
            this.Id = id;
            this.announced = announced;
            this.status = status;
        }

        public string Announced { get => announced; set => announced = value; }
        public string Status { get => status; set => status = value; }
        public int Id { get => id; set => id = value; }
    }


}
