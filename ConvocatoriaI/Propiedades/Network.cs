﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvocatoriaI.Propiedades
{
    class Network
    {
        private int id;
        private string technology;

        public Network()
        {
        }

        public Network(int id, string technology)
        {
            this.id = id;
            this.Technology = technology;
        }

        public string Technology { get => technology; set => technology = value; }
        public int Id { get => id; set => id = value; }
    }
}
