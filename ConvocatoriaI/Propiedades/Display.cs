﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvocatoriaI.Propiedades
{
    class Display
    {
        private int id;
        private string type;
        private string size;
        private string resolution;
        private string protection;


        public Display()
        {
        }

        public Display(int id, string type, string size, string resolution, string protection)
        {
            this.id = id;
            this.type = type;
            this.size = size;
            this.resolution = resolution;
            this.protection = protection;
        }

        public string Type { get => type; set => type = value; }
        public string Size { get => size; set => size = value; }
        public string Resolution { get => resolution; set => resolution = value; }
        public string Protection { get => protection; set => protection = value; }
        public int Id { get => id; set => id = value; }
    }
}
