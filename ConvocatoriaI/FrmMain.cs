﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvocatoriaI
{
    public partial class FrmMain : Form
    {

        public FrmMain()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            FrmRegistroCelulares frc = new FrmRegistroCelulares();
            frc.TblCelular = dsCelular.Tables["Celular"];
            frc.DsCelular = dsCelular;
            frc.TblBody = dsCelular.Tables["Body"];
            frc.DsBody = dsCelular;
            frc.TblDisplay = dsCelular.Tables["Display"];
            frc.DsDisplay = dsCelular;
            frc.TblLaunch = dsCelular.Tables["Launch"];
            frc.DsLaunch = dsCelular;
            frc.TblMemory = dsCelular.Tables["Memory"];
            frc.DsMemory = dsCelular;
            frc.TblNet = dsCelular.Tables["Networking"];
            frc.DsNet = dsCelular;
            frc.TblPlatform = dsCelular.Tables["Platform"];
            frc.DsPlatfrom = dsCelular;
            frc.ShowDialog();
            
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }

        private void CelularesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTablaCelulares ftc = new FrmTablaCelulares();
            ftc.MdiParent = this;
            ftc.DsCelular = dsCelular;
            ftc.Show();
        }
    }
}
