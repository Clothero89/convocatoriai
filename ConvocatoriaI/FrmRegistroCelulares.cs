﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvocatoriaI
{
    public partial class FrmRegistroCelulares : Form
    {
        //Tabla Celular
        private DataTable tblCelular;
        private DataSet dsCelular;
        private BindingSource bsCelular;
        private DataRow drCelular;

        public DataTable TblCelular { get => tblCelular; set => tblCelular = value; }
        public DataSet DsCelular { get => dsCelular; set => dsCelular = value; }

        //Tabla Network
        private DataTable tblNet;
        private DataSet dsNet;
        private BindingSource bsNet;
        private DataRow drNet;

        public DataTable TblNet { get => tblNet; set => tblNet = value; }
        public DataSet DsNet { get => dsNet; set => dsNet = value; }

        //Tabla Launch
        private DataTable tblLaunch;
        private DataSet dsLaunch;
        private BindingSource bsLaunch;
        private DataRow drLaunch;

        public DataTable TblLaunch { get => tblLaunch; set => tblLaunch = value; }
        public DataSet DsLaunch { get => dsLaunch; set => dsLaunch = value; }

        //Tabla Body
        private DataTable tblBody;
        private DataSet dsBody;
        private BindingSource bsBody;
        private DataRow drBody;

        public DataTable TblBody { get => tblBody; set => tblBody = value; }
        public DataSet DsBody { get => dsBody; set => dsBody = value; }

        //Tabla Display
        private DataTable tblDisplay;
        private DataSet dsDisplay;
        private BindingSource bsDisplay;
        private DataRow drDisplay;

        public DataTable TblDisplay { get => tblDisplay; set => tblDisplay = value; }
        public DataSet DsDisplay { get => dsDisplay; set => dsDisplay = value; }

        //Tabla Platform
        private DataTable tblPlatform;
        private DataSet dsPlatform;
        private BindingSource bsPlatfrom;
        private DataRow drPlatform;

        public DataTable TblPlatform { get => tblPlatform; set => tblPlatform = value; }
        public DataSet DsPlatfrom { get => dsPlatform; set => dsPlatform = value; }

        //Tabla Memory
        private DataTable tblMemory;
        private DataSet dsMemory;
        private BindingSource bsMemory;
        private DataRow drMemory;

        public DataTable TblMemory { get => tblMemory; set => tblMemory = value; }
        public DataSet DsMemory { get => dsMemory; set => dsMemory = value; }



        public FrmRegistroCelulares()
        {
            InitializeComponent();
            bsCelular = new BindingSource();
            bsNet = new BindingSource();
            bsLaunch = new BindingSource();
            bsBody = new BindingSource();
            bsDisplay = new BindingSource();
            bsPlatfrom = new BindingSource();
            bsMemory = new BindingSource();
        }

        public DataRow DrCelular
        {
            set
            {
                drCelular = value;
                txtMarca.Text = drCelular["Marca"].ToString();
                txtModelo.Text = drCelular["Modelo"].ToString();
            }
        }

        public DataRow DrNet
        {
            set
            {
                drNet = value;
                txtTech.Text = drCelular["Technology"].ToString();
            }
        }

        public DataRow DrLaunch
        {
            set
            {
                drLaunch = value;
                txtAnnoun.Text = drLaunch["Announced"].ToString();
                txtStatus.Text = drLaunch["Status"].ToString();
            }
        }

        public DataRow DrBody
        {
            set
            {
                drBody = value;
                txtDim.Text = drBody["Dimensions"].ToString();
                txtWei.Text = drBody["Weight"].ToString();
                txtBuild.Text = drBody["Build"].ToString();
                txtSim.Text = drBody["SIM"].ToString();
            }
        }

        public DataRow DrDisplay
        {
            set
            {
                drDisplay = value;
                txtType.Text = drDisplay["Type"].ToString();
                txtSize.Text = drDisplay["Size"].ToString();
                txtReso.Text = drDisplay["Resolution"].ToString();
                txtProt.Text = drDisplay["Protection"].ToString();
            }
        }

        public DataRow DrPlatform
        {
            set
            {
                drPlatform = value;
                txtOS.Text = drPlatform["OS"].ToString();
                txtChipset.Text = drPlatform["Chipset"].ToString();
                txtCpu.Text = drPlatform["CPU"].ToString();
                txtGpu.Text = drPlatform["GPU"].ToString();
            }
        }


        public DataRow DrMemory
        {
            set
            {
                drMemory = value;
                txtCSlot.Text = drMemory["Card Slot"].ToString();
                txtInt.Text = drMemory["Internal"].ToString();
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string marca, modelo,tech, announ, status, dim, weig, build, sim, type, size, resolution, protection, os, chipset, cpu, gpu, cardSlot, inter;

            marca = txtMarca.Text;
            modelo = txtModelo.Text;
            tech = txtTech.Text;
            announ = txtAnnoun.Text;
            status = txtStatus.Text;
            dim = txtDim.Text;
            weig = txtWei.Text;
            build = txtBuild.Text;
            sim = txtSim.Text;
            type = txtType.Text;
            size = txtSize.Text;
            resolution = txtReso.Text;
            protection = txtProt.Text;
            os = txtOS.Text;
            chipset = txtChipset.Text;
            cpu = txtCpu.Text;
            gpu = txtGpu.Text;
            cardSlot = txtCSlot.Text;
            inter = txtInt.Text;

            if (drCelular != null)
            {
                DataRow drNew = TblCelular.NewRow();
                int index = TblCelular.Rows.IndexOf(drCelular);

                drNew["Id"] = drCelular["Id"];
                drNew["Marca"] = marca;
                drNew["Modelo"] = modelo;

                TblCelular.Rows.RemoveAt(index);
                TblCelular.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblCelular.Rows.Add(TblCelular.Rows.Count + 1, marca, modelo);
            }

            if (drNet != null)
            {
                DataRow drNew = TblNet.NewRow();
                int index = TblNet.Rows.IndexOf(drNet);

                drNew["Id"] = drNet["Id"];
                drNew["Technology"] = tech;

                TblNet.Rows.RemoveAt(index);
                TblNet.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblNet.Rows.Add(TblNet.Rows.Count + 1, tech);
            }

            if (drLaunch != null)
            {
                DataRow drNew = TblLaunch.NewRow();
                int index = TblLaunch.Rows.IndexOf(drLaunch);

                drNew["Id"] = drLaunch["Id"];
                drNew["Announced"] = announ;
                drNew["Status"] = status;

                TblLaunch.Rows.RemoveAt(index);
                TblLaunch.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblLaunch.Rows.Add(TblLaunch.Rows.Count + 1, announ, status);
            }

            if (drBody != null)
            {
                DataRow drNew = TblBody.NewRow();
                int index = TblBody.Rows.IndexOf(drBody);

                drNew["Id"] = drBody["Id"];
                drNew["Dimensions"] = dim;
                drNew["Weight"] = weig;
                drNew["Build"] = build;
                drNew["SIM"] = sim;

                TblBody.Rows.RemoveAt(index);
                TblBody.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblBody.Rows.Add(TblBody.Rows.Count + 1, dim, weig, build, sim);
            }

            if (drDisplay != null)
            {
                DataRow drNew = TblDisplay.NewRow();
                int index = TblDisplay.Rows.IndexOf(drCelular);

                drNew["Id"] = drDisplay["Id"];
                drNew["Type"] = type;
                drNew["Size"] = size;
                drNew["Resolution"] = resolution;
                drNew["Protection"] = protection;

                TblDisplay.Rows.RemoveAt(index);
                TblDisplay.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblDisplay.Rows.Add(TblDisplay.Rows.Count + 1, type, size, resolution, protection);
            }

            if (drPlatform != null)
            {
                DataRow drNew = TblPlatform.NewRow();
                int index = TblPlatform.Rows.IndexOf(drCelular);

                drNew["Id"] = drPlatform["Id"];
                drNew["OS"] = os;
                drNew["Chipset"] = chipset;
                drNew["CPU"] = cpu;
                drNew["GPU"] = gpu;

                TblPlatform.Rows.RemoveAt(index);
                TblPlatform.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblPlatform.Rows.Add(TblPlatform.Rows.Count + 1, os, chipset, cpu, gpu);
            }

            if (drMemory != null)
            {
                DataRow drNew = TblMemory.NewRow();
                int index = TblMemory.Rows.IndexOf(drMemory);

                drNew["Id"] = drMemory["Id"];
                drNew["Card Slot"] = cardSlot;
                drNew["Internal"] = inter;

                TblMemory.Rows.RemoveAt(index);
                TblMemory.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblMemory.Rows.Add(TblMemory.Rows.Count + 1, cardSlot, inter);
            }

            Dispose();
        }
    }
}
