﻿namespace ConvocatoriaI
{
    partial class FrmTablaCelulares
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMemory = new System.Windows.Forms.Button();
            this.btnPlatform = new System.Windows.Forms.Button();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.btnBody = new System.Windows.Forms.Button();
            this.btnLaunch = new System.Windows.Forms.Button();
            this.btnNetwork = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFinder = new System.Windows.Forms.TextBox();
            this.dgvCelulares = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCelulares)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnMemory);
            this.flowLayoutPanel1.Controls.Add(this.btnPlatform);
            this.flowLayoutPanel1.Controls.Add(this.btnDisplay);
            this.flowLayoutPanel1.Controls.Add(this.btnBody);
            this.flowLayoutPanel1.Controls.Add(this.btnLaunch);
            this.flowLayoutPanel1.Controls.Add(this.btnNetwork);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 378);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(860, 46);
            this.flowLayoutPanel1.TabIndex = 11;
            // 
            // btnMemory
            // 
            this.btnMemory.Location = new System.Drawing.Point(756, 4);
            this.btnMemory.Margin = new System.Windows.Forms.Padding(4);
            this.btnMemory.Name = "btnMemory";
            this.btnMemory.Size = new System.Drawing.Size(100, 28);
            this.btnMemory.TabIndex = 1;
            this.btnMemory.Text = "Ver Memory";
            this.btnMemory.UseVisualStyleBackColor = true;
            this.btnMemory.Click += new System.EventHandler(this.BtnMemory_Click);
            // 
            // btnPlatform
            // 
            this.btnPlatform.Location = new System.Drawing.Point(648, 4);
            this.btnPlatform.Margin = new System.Windows.Forms.Padding(4);
            this.btnPlatform.Name = "btnPlatform";
            this.btnPlatform.Size = new System.Drawing.Size(100, 28);
            this.btnPlatform.TabIndex = 2;
            this.btnPlatform.Text = "Ver Platform";
            this.btnPlatform.UseVisualStyleBackColor = true;
            this.btnPlatform.Click += new System.EventHandler(this.BtnPlatform_Click);
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(540, 4);
            this.btnDisplay.Margin = new System.Windows.Forms.Padding(4);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(100, 28);
            this.btnDisplay.TabIndex = 3;
            this.btnDisplay.Text = "Ver Display";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.BtnDisplay_Click);
            // 
            // btnBody
            // 
            this.btnBody.Location = new System.Drawing.Point(432, 4);
            this.btnBody.Margin = new System.Windows.Forms.Padding(4);
            this.btnBody.Name = "btnBody";
            this.btnBody.Size = new System.Drawing.Size(100, 28);
            this.btnBody.TabIndex = 4;
            this.btnBody.Text = "Ver Body";
            this.btnBody.UseVisualStyleBackColor = true;
            this.btnBody.Click += new System.EventHandler(this.BtnBody_Click);
            // 
            // btnLaunch
            // 
            this.btnLaunch.Location = new System.Drawing.Point(324, 4);
            this.btnLaunch.Margin = new System.Windows.Forms.Padding(4);
            this.btnLaunch.Name = "btnLaunch";
            this.btnLaunch.Size = new System.Drawing.Size(100, 28);
            this.btnLaunch.TabIndex = 5;
            this.btnLaunch.Text = "Ver Launch";
            this.btnLaunch.UseVisualStyleBackColor = true;
            this.btnLaunch.Click += new System.EventHandler(this.BtnLaunch_Click);
            // 
            // btnNetwork
            // 
            this.btnNetwork.Location = new System.Drawing.Point(216, 4);
            this.btnNetwork.Margin = new System.Windows.Forms.Padding(4);
            this.btnNetwork.Name = "btnNetwork";
            this.btnNetwork.Size = new System.Drawing.Size(100, 28);
            this.btnNetwork.TabIndex = 6;
            this.btnNetwork.Text = "Ver Network";
            this.btnNetwork.UseVisualStyleBackColor = true;
            this.btnNetwork.Click += new System.EventHandler(this.BtnNetwork_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Búsqueda";
            // 
            // txtFinder
            // 
            this.txtFinder.Location = new System.Drawing.Point(13, 33);
            this.txtFinder.Margin = new System.Windows.Forms.Padding(4);
            this.txtFinder.Name = "txtFinder";
            this.txtFinder.Size = new System.Drawing.Size(855, 22);
            this.txtFinder.TabIndex = 9;
            // 
            // dgvCelulares
            // 
            this.dgvCelulares.AllowUserToAddRows = false;
            this.dgvCelulares.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCelulares.Location = new System.Drawing.Point(12, 65);
            this.dgvCelulares.Margin = new System.Windows.Forms.Padding(4);
            this.dgvCelulares.Name = "dgvCelulares";
            this.dgvCelulares.RowHeadersWidth = 51;
            this.dgvCelulares.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCelulares.Size = new System.Drawing.Size(857, 310);
            this.dgvCelulares.TabIndex = 8;
            // 
            // FrmTablaCelulares
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 427);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFinder);
            this.Controls.Add(this.dgvCelulares);
            this.Name = "FrmTablaCelulares";
            this.Text = "FrmTablaCelulares";
            this.Load += new System.EventHandler(this.FrmTablaCelulares_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCelulares)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnMemory;
        private System.Windows.Forms.Button btnPlatform;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.Button btnBody;
        private System.Windows.Forms.Button btnLaunch;
        private System.Windows.Forms.Button btnNetwork;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFinder;
        private System.Windows.Forms.DataGridView dgvCelulares;
    }
}